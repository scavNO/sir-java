/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Dag Østgulen Heradstveit
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.devbugger.modules;

import com.devbugger.io.Response;

/**
 * This is the interface that all modules must
 * implement in order to work with this bot.
 *
 * It provides the basic input and output operations of a module
 * which is to accept input, and provide a io using {@link com.devbugger.io.Response}
 */
public interface SirJavaModule {

    /**
     * Every module needs to define a trigger that will cause
     * it to spring into action. It will also need to document
     * clearly which trigger word it uses.
     * @return the command that will trigger the module
     */
    public String getCommand();

    /**
     * Every module is responsible for creating its own response
     * that will be sent to the location of origin.
     * However, all responses has to be mapped to the {@link com.devbugger.io.Response} class
     * to make sure that they follow the IRC protocol standard of message.
     *
     * This method should also contain a returnMessage which holds the
     * defined response for the given module, else it will be returned an error to the chat.
     * @param input the full event from the socket.
     * @return a well formed response for the IRC protocol
     */
    public default Response getResponse(String input) {
        return new Response(input, "Not Implemented yet");
    }


}
