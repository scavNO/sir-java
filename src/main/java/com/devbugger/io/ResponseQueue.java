/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Dag Østgulen Heradstveit
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.devbugger.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the io queue for the bot.
 * It tracks all queries that have arrived for the bot
 * that it needs to respond to.
 *
 * It run in its own separate thread to avoid blocking
 * the socket stream to the Connect server.
 */
public final class ResponseQueue implements Runnable {

    private final List<Response> responseList;
    private final BufferedWriter bufferedWriter;
    private final Thread queueThread;
    private volatile boolean running = true;

    public ResponseQueue(BufferedWriter bufferedWriter) {
        queueThread = new Thread(this, "queryQueue");
        responseList = new ArrayList<>();
        this.bufferedWriter = bufferedWriter;
    }

    /**
     * Start the thread after the constructor complete its
     * work to avoid problems with this leaks etc.
     */
    public void start() {
        this.queueThread.start();
    }

    /**
     * Add a new {@link Response}
     * to the {@link ResponseQueue}
     * @param response the question to be added to the queue
     */
    public void add(Response response) {
        responseList.add(response);
    }

    /**
     * Set the status of the thread.
     * @param running false to kill thread.
     */
    public void setRunning(boolean running) {
        this.running = running;
    }

    /**
     * Provide basic flood protection for the bot.
     * This means that if a lot of !io stuff is being done
     * to the bot, it will create a queue for it.
     */
    @Override
    public void run() {
        while(running) {
            try {
                //I have no idea why, but the thread needs to sleep
                //before it starts running else code will never exec.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(!responseList.isEmpty()) {
                Response q = responseList.get(0);
                if (System.currentTimeMillis() - q.getTime() > 2000) {
                    try {
                        bufferedWriter.write(q.getResponse());
                        bufferedWriter.flush();
                        responseList.remove(q);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}