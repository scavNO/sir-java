/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Dag Østgulen Heradstveit
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.devbugger.io;

import com.devbugger.protocol.Protocol;

/**
 * Every io the bot returns back to any given chat
 * is in the form of a {@link com.devbugger.io.Response} object.
 * This object created the entire message based on input.
 */
public final class Response {

    private final long time;
    private final String channel;
    private final String response;

    /**
     * To return the io to the source, return a String
     * following this pattern:
     *  PRIVMSG #CHAN :MESSAGE \r\n
     *
     * @param input the raw socket input
     * @param returnMessage the message to return to the socket
     */
    public Response(String input, String returnMessage) {
        this.time = System.currentTimeMillis();
        this.channel = getSourceChan(input);
        this.response = returnMessage;
    }

    /**
     * Gets the #CHAN representation of the given channel.
     * @param input raw socket input
     * @return a proper #CHAN name
     */
    private String getSourceChan(String input) {
        String chan = input.substring(input.indexOf("#"));
        return chan.substring(0, chan.indexOf(" "));
    }

    /**
     * The time the {@link com.devbugger.io.Response} was created.
     * @return creation time in milliseconds
     */
    public long getTime() {
        return time;
    }

    /**
     * Uses {@link com.devbugger.protocol.Protocol} to provide the correct
     * formatting for the Connect socket message.
     * @return s String for the active {@link java.net.Socket}.
     */
    public String getResponse() {
        return (Protocol.PRIVMSG.toString() + this.channel + " :" + this.response + "\r\n");
    }
}