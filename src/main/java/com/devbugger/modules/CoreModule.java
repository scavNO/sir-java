/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Dag Østgulen Heradstveit
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.devbugger.modules;

import com.devbugger.io.Response;

/**
 * This is the primary module of the bot.
 * It contains all the core functionality that could be expceted
 * from the bot such as commands for qutting/disconnecting.
 */
public class CoreModule implements SirJavaModule{

    private final String command = ".sirjava";
    private String returnMessage = "Sir Java is a bot.";

    @Override
    public String getCommand() {
        return command;
    }

    @Override
    public Response getResponse(String input) {
        returnMessage = createResponse(input);
        return new Response(input, returnMessage);
    }

    private String createResponse(String input) {
        input = input.substring(input.indexOf(".sirjava") + 1);
        String command[] = input.split(" ");

        if(command.length == 1) {
            return "Please specify a command or use .sirjava help";
        }

        else if(command[1].equals("quit")) {
            return "You want to quit.";
        }

        return null;
    }


}
