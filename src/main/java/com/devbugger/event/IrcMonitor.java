/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Dag Østgulen Heradstveit
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.devbugger.event;

import com.devbugger.io.Response;
import com.devbugger.modules.CoreModule;
import com.devbugger.modules.HelpModule;
import com.devbugger.modules.SirJavaModule;
import com.devbugger.protocol.PingPong;
import com.devbugger.protocol.Protocol;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements the {@link IrcListener} interface
 * to provide a monitor of the socket traffic and server as an
 * entry point into the application itself.
 */
public class IrcMonitor implements IrcListener {

    /**
     * Flag to define whether or not the bot is supposed to
     * keep its running loop going or if it should start
     * killing threads and closing the socket.
     */
    private boolean isRunning = true;

    /**
     * The message to return to the socket.
     */
    private String socketReturnMessage = "";

    /**
     * {@link com.devbugger.io.Response} object that will be returned to
     * the socket containing everything necessary to
     * respond to the correct place for the IRC socket event.
     */
    private Response response;

    /**
     * Modules to be loaded at run time.
     * This needs to be fixed, but currently just playing around with ideas.
     * TODO: Defines the list of modules. This needs a "hook" somewhere
     */
    public List<SirJavaModule> classList = new ArrayList<>();

    public IrcMonitor() {
        classList.add(new HelpModule());
        classList.add(new CoreModule());
    }

    @Override
    public void socketUpdate(IrcSocketEvent ircSocketEvent) {
        socketReturnMessage = "";
        System.out.println(ircSocketEvent.getIrcSocketEvent());

        if(ircSocketEvent.getIrcSocketEvent().contains(Protocol.CONNECTFAIL_NICKINUSE.toString())) {
            System.out.println("Nickname in use");
        }

        if(ircSocketEvent.getIrcSocketEvent().contains(Protocol.CONNECT_OK.toString())) {
            System.out.println("Connected");
        }

        if(ircSocketEvent.getIrcSocketEvent().contains(Protocol.QUIT.toString())) {
            socketReturnMessage = Protocol.CLIENT_QUIT.toString();
            isRunning = false;
        }

        //Modules assume controll of execution flow.
        for(SirJavaModule module : classList) {
            if(ircSocketEvent.getIrcSocketEvent().contains(module.getCommand())) {
                response = module.getResponse(ircSocketEvent.getIrcSocketEvent());
            }
        }


        if(ircSocketEvent.getIrcSocketEvent().contains(Protocol.PING.toString())) {
            socketReturnMessage = new PingPong(ircSocketEvent.getIrcSocketEvent()).getPong();
        }

        if((ircSocketEvent.getIrcSocketEvent().contains(Protocol.QUESTION.toString())
                && (!ircSocketEvent.getIrcSocketEvent().contains(Protocol.QUIT.toString())))) {
            socketReturnMessage = ircSocketEvent.getIrcSocketEvent();
        }


    }

    /**
     * Return true for as long as the bot should remain active
     * and running.
     * @return bot running status
     */
    public boolean isRunning() {
        return isRunning;
    }

    /**
     * When events occur, they sometimes contains feedback
     * to the socket and these messages will be contained here.
     * @return socket return messages
     */
    public String getSocketReturnMessage() {
        return socketReturnMessage;
    }

    public Response getResponse() {
        return response;
    }
}