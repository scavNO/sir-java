/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Dag Østgulen Heradstveit
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.devbugger.event;

import java.util.ArrayList;
import java.util.List;

/**
 * Events coming from the Connect network the bot it is
 * connected to. This is a simple object which is updated
 * for each socket ircSocketEvent.
 */
public class Irc {

    /**
     * Updates are seen as new lines of text arriving
     * over the socket connection to the Connect server.
     */
    private String ircSocketEvent;

    private List<IrcListener> ircListeners;

    public Irc() {
        this.ircSocketEvent = "";
        this.ircListeners = new ArrayList<>();
    }

    public String getIrcSocketEvent() {
        return ircSocketEvent;
    }

    public void setIrcSocketEvent(String ircSocketEvent) {
        this.ircSocketEvent = ircSocketEvent;
        this.fireEventChanged();
    }

    public void addIrcListener(IrcListener ircListener)     {
        this.ircListeners.add(ircListener);
    }

    public void removeIrcListener(IrcListener ircListener) {
        this.ircListeners.remove(ircListener);
    }

    /**
     * Fire the event on the object where in turn a new
     * {@link IrcSocketEvent} is added
     * to the {@link IrcListener}
     */
    private void fireEventChanged() {
        IrcSocketEvent ircSocketEvent = new IrcSocketEvent(this, this.getIrcSocketEvent());

        for(IrcListener ircListener : this.ircListeners) {
            ircListener.socketUpdate(ircSocketEvent);
        }
    }
}