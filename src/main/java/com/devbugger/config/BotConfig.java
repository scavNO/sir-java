/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Dag Østgulen Heradstveit
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.devbugger.config;

import java.io.*;
import java.util.Properties;

/**
 * This is the representation of all the properties
 * the bot holds. It is okay to look at this as "the bot"
 * since this is where it pretty much comes alive.
 */
public class BotConfig {

    private final String host;
    private final int port;
    private final String nick;
    private final String altNick;
    private final String user;
    private final String pass;

    public BotConfig() throws IOException {
        InputStream input = new FileInputStream("config.properties");
        Properties property = new Properties();
        property.load(input);

        this.nick = "NICK " + property.getProperty("nick")+"\r\n";
        this.altNick = "NICK " + property.getProperty("altnick")+"\r\n";
        this.user = "USER " + property.getProperty("user") + " 8 * : " + property.getProperty("user") +"\r\n";
        this.pass = "PASS " + property.getProperty("pass")+"\r\n";

        this.host = property.getProperty("host");
        this.port = Integer.parseInt(property.getProperty("port"));
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getNick() {
        return nick;
    }

    public String getAltNick() {
        return altNick;
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }
}