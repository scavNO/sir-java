/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Dag Østgulen Heradstveit
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.devbugger.data;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Contains the default data used by the bot.
 * This is information about java runtime, authors, bot version
 * and what the bot is providing a description plus a link to its repository.
 */
public final class KnowledgeBase {

    private final Map<String, String> keywords;

    public KnowledgeBase() {
        keywords = new HashMap<>();
        keywords.put("java", System.getProperty("java.version"));
        keywords.put("version", "sir-java-0.0.1-SNAPSHOT");
        keywords.put("sirjava", "A simple Java implementation of a Connect bot - https://bitbucket.org/scavNO/sir-java/src");
        keywords.put("authors", "scav");
    }

    /**
     * Returns true if the key is part of the set.
     * This is useful to see if the {@link com.devbugger.io.Response} is contained
     * within {@link com.devbugger.data.KnowledgeBase} or if it should be sent to a
     * different handler.
     * @param key the key we want to check for
     * @return boolean value of whether or not the key exists
     */
    public boolean hasKey(String key) {
        Set<String> keys = keywords.keySet();

        return keys.stream()
                .anyMatch(k -> k.equals(key));
    }

    public String getKeyword(String key) {
        return keywords.get(key);
    }
}