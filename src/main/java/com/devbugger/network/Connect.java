/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Dag Østgulen Heradstveit
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.devbugger.network;

import com.devbugger.config.BotConfig;
import com.devbugger.data.Channels;
import com.devbugger.event.Irc;
import com.devbugger.event.IrcMonitor;
import com.devbugger.io.ResponseQueue;

import java.io.*;
import java.net.Socket;

/**
 * This class creates the connection to the Protocol server
 * that Sir Java will hang out on.
 * It sets up everything the botConfig needs to run.
 *
 * Primary class for {@link com.devbugger.SirJava}
 */
public final class Connect {

    private final Socket ircSocket;
    private final BotConfig botConfig = new BotConfig();
    private final IrcMonitor ircMonitor;
    private final Irc irc;

    public Connect() throws IOException {
        ircMonitor = new IrcMonitor();
        irc = new Irc();
        irc.addIrcListener(ircMonitor);
        this.ircSocket = new Socket(botConfig.getHost(), botConfig.getPort());
    }

    /**
     * This is where the bot primarily runs.
     * Everything else needs to be hooked into this method.
     */
    public void runBot() {
        try(BufferedReader server = new BufferedReader(
                new InputStreamReader(ircSocket.getInputStream()));
            BufferedWriter client = new BufferedWriter(
                    new OutputStreamWriter(ircSocket.getOutputStream()))) {

            runningBot(server, client);

        } catch (IOException e) {
            e.printStackTrace();
        }

        //Bot has closed sockets and stopped all threads.
        System.exit(0);
    }

    private void runningBot(BufferedReader server, BufferedWriter client) throws IOException {

        //Set up the bot for connecting
        client.write(botConfig.getNick());
        client.write(botConfig.getUser());
        client.flush();

        //Join channels
        for(String channel : new Channels().getChans()) {
            client.write(channel);
        }
        client.flush();

        ResponseQueue responseQueue = new ResponseQueue(client);
        responseQueue.start();

        //Start the bot
        String serverMessage;
        while((serverMessage = server.readLine()) != null) {
            irc.setIrcSocketEvent(serverMessage);

            if(ircMonitor.isRunning()) {
                if(ircMonitor.getResponse() != null) {
                    responseQueue.add(ircMonitor.getResponse());
                }
            }

            else {
                responseQueue.setRunning(false);
                client.write(ircMonitor.getSocketReturnMessage());
                client.flush();
                ircSocket.close();
                break;
            }
        }
    }
}