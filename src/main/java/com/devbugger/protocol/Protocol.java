/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Dag Østgulen Heradstveit
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.devbugger.protocol;

/**
 * Contains different constant values for the Protocol
 * protocol and bot specific constants providing access to
 * features, triggering events and attempt to make code
 * easier to read and maintain.
 *
 * Notice that the white spaces here are very important to maintain
 * RFC 2812 / Protocol client protocol integrity.
 */
public enum Protocol {

    //BOT PROTOCOL
    PING("PING "),
    QUESTION(":!"),
    QUIT(":!Quit"),

    //Connect PROTOCOL
    CONNECT_OK("004"),
    CONNECTFAIL_NICKINUSE("433"),
    PRIVMSG("PRIVMSG "),
    CLIENT_QUIT("QUIT :Socket and threads closed.\r\n");

    private final String value;

    private Protocol(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}