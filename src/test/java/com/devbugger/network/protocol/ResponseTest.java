/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Dag Østgulen Heradstveit
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.devbugger.network.protocol;

import com.devbugger.io.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnit4.class)
public class ResponseTest {

    /**
     * Defines a typical incoming string from the Connect socket
     * that we will have to handle with the bot.
     */
    public final String input = ":scav!scav@devbugger.com PRIVMSG #sirjava :!java";
    public final String response = "Test response";

    @Test()
    public void testCommandWords() throws Exception {
        System.out.print(new Response(input, response).getResponse());
    }

    @Test()
    public void testChanReturn() throws Exception  {
        Response q = new Response(input, response);
        assertThat(q.getResponse(), is("PRIVMSG #sirjava :Now in version 8! The best around\r\n"));
    }

    @Test()
    public void testParameter() throws Exception  {
        String input = ":scav!scav@devbugger.com PRIVMSG #sirjava :.sirjava quit";
        input = input.substring(input.indexOf(".sirjava") + 1);
        String a[] = input.split(" ");
        for(String z : a) {
            System.out.println(z);
        }
    }
}
